class: center, middle
# Подготовка к работе
---


### Основы работы с git
* git init - инициализация репозитория
* git add - добавление файлов в индекс
* git commit - фиксация изменений в файлах
* git push - отправка изменений на удалённый репозиторий
* git pull - получение изменений с удалённого репозитория
* git status - отобразить текущий статус репозитория
---


### Основы работы с virtualenv
virtualenv - инструмент для создания изолированных окружений для проектов на Python. Виртуальное окружение - папка которая содержит все нужные пакеты и исполняемые файлы для данного проекта.
virtualenv -p python2.7 {project_name}

mkvirtualenv - оболочка для virtualenv, добавляющая полезные команды, упрощающая работу с виртуальными окружениями.

Команды:
- mkrvirtualenv {project_name} - создание виртуального окружения
- workon {project_name} - активация виртуального окружения
- deactivate - деактивация
- rmrvirtualenv {project_name} - удалениe
- lsrvirtualenv - список
---


### Установка Pycharm
- [Скачать](https://www.jetbrains.com/pycharm/download/#section=linux)
- Распаковать, положить в home
- Добавить в .bashrc export PATH=~/pycharm/bin:$PATH (для маководов .bash_profile)
---

### Настройка виртуального окружения
- pip install virtualenvwrapper
- mkdir ~/.venvs
- Добавить в .bashrc export WORKON_HOME=~/.venvs
- Добавить в .bashrc source /usr/local/bin/virtualenvwrapper.sh
---


### Создание Django-проекта
- workon {project_name}
- pip install django==1.11
- django-admin startproject {project_name}
- заходим в папку с проектом
- ./manage.py migrate
- ./manage.py runserver
---


### Работа с проектом в PyCharm
- Открываем проект
- Конфигурируем интерпретатор: Preferences -> Project -> Project Interpeter -> Шестерёнка -> Add Local -> В папке с виртуальным окружением проекта выбираем python2.7
- Конфигурируем поддержку Django в проекте: Preferences -> Languages & Frameworks -> Django -> Включаем галочку если выключена -> Прописываем все нужные пути
- Запускаем проект
