class: center, middle
# Роутинг
---


### Роутинг
По умолчанию корневая конфигурация роутера находится в файле {project_name}/urls.py

Конфигурация представляет из себя список с названием urlpatterns состоящий из объектов django.config.urls.url()

Например:
```py
urlpatterns = [
    url(r'^{регулярное выражение}$', {view-функция}),
    url(r'^{другое регулярное выражение}$', {другая view-функция}),
]
```
---


### Как Django обрабатывает запрос
- Определяет модуль где хранится корневая конфигурация роутера (ROOT_URLCONF)
- Загружает модуль, ищет urlpatterns
- Проходит через все паттерны пока URL запроса не совпадёт с одним из паттернов
- После совпадения вызывается view-функция со следующими аргументами:
    * объект HttpRequest
    * позиционные аргументы полученые из неименованных групп регулярного выражения или именованные аргументы полученые из именованных групп
- Если не было совпадений возвращается ошибка
---


### Пример конфигурации роутера
```py
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^articles/2003/$',
        views.special_case_2003, name='special_2003'),
    url(r'^articles/([0-9]{4})/$',
        views.year_archive, name='year_archive'),
    url(r'^articles/([0-9]{4})/([0-9]{2})/$',
        views.month_archive, name='month_archive'),
    url(r'^articles/(?P<slug>[\w-]+)/$',
        views.article_detail, name='article_detail')
]
```
---


### Примеры запросов
- /articles/2005/03/
- /articles/2005/3/
- /articles/2003/
- /articles/2003
- /articles/statya-o-chem-to-tam/


???
- 3 паттерн - views.month_archive(request, '2005', '03')
- нет совпадений
- 1 паттерн - views.special_case_2003(request)
- нет совпадений
- последний паттерн - views.article_detail(request, slug='statya-o-chem-to-tam')
---


### Маршрутизация в проекте
Список роутов в большом проекте можно и нужно разбивать на части

Обычно разбиение идёт по приложениям

```py
urlpatterns = [
    url (r'^$', home, name='home')
    url (r'^articles/', include('articles.urls'))
    url (r'^admin/', include('admin.site.urls'))
]
```
---


### Обратный роутинг
При объявлении роута можно указать его имя, это нужно для того чтобы можно было сгенерировать урлу внутри кода.

В python-коде
```py
from django.core.urlresolvers import reverse
reverse('special_2003')
reverse('month_archive', args=(2014, 3))
reverse('article_detail', kwargs={'slug': 'statya-kakaya-to'})
```

В шаблоне
```html
{% url 'article_detail' 'statya-kakaya-to' %}
```
---
