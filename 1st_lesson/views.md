class: center, middle
# Вьюхи
---


### View
View-функция или просто view - это обычная функция которая принимает веб-запрос и отдаёт веб-ответ

```py
def имя_функции(запрос):
    return ответ
```

```py
def article_detail(request, slug):
    try:
        article = Article.objects.get(slug=slug)
    except Article.DoesNotExist:
        raise Http404

    return HttpResponse(article.text)
```
---


### Веб-запросы и веб-ответы в Django
В Djano для инкапсуляции всего что есть у веб-запроса и веб-ответа существует два класса:
- django.http.HtppRequest
- django.http.HtppResponse
---


### HttpRequest
- request.method - метод запроса
- request.GET - словарь с GET параметрами
- request.POST - словарь с POST параметрами
- request.COOKIES - словарь с Cookie
- request.FILES - загруженные файлы
- request.META - словарь с HTTP-заголовками
- request.session - словарь с текущей сессией
- request.user - текущий пользователь

???
- Словарь с сессией живет от запроса к запросу
- Все словари выше живут в рамказ запроса
---


### HttpResponse
- Создание объекта
```py
response = Response("HelloWorld!!!",
                        content_type='application/json'
                        status=200)
```
- Добавление заголовков
```py
response['Какой-то заголовок'] = 'Значение'
```
- Выставление cookie
```py
response.set_cookie('имя-куки', 'значение')
```
- Остальные методы можно найти в документации
---


### Специальные типы HttpResponse
- HttpResponseRedirect - status=302
- HttpResponsePermanentRedirect - status=301
- HttpResponseBadRequest - status=400
- HttpResponseNotFound - status=404
- HttpResponseForbidden - status=403
---


### Передача параметров из урлов во вьюхи
- Через GET параметры /some/url/?id=1
```py
request.GET.get('id')      #id = 1
request.GET.getlist('id')  #id = [1, 2, 3]
```
- Через URL как позиционные аргументы (неименнованые группы)
```py
url(r'^some/url/(\d+)/$')
...
def some_view(request, id)
```
- Через URL как именованные аргументы (именнованые группы)
```py
url(r'^some/url/(?P<id>\d+)/$')
...
def some_view(request, id)
```
- Можно сделать вьюху у которой параметры будут прописаны неявно
```py
def some_view(request, *args, **kwargs):
    id = args(0)
    id = kwargs('id')
```
---
