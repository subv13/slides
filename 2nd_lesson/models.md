class: center, middle
# Модели
---


### Модели в Django
Модель в Django это python-класс который наследуется от django.db.models.Model

```py
from django.db import models

class Article(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    is_enabled = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
```
---


### Django ORM
ORM (Object-Relational Mapping, Объектно-реляционное отображение) - отображение базы данных на концепции ООП, высокоуровневая абстракция позволяющая писать python код вместо SQL запросов

Django ORM:
- Каждая модель (класс) - отдельная таблица в базе данных
- Каждый поле класса - колонка в таблице
- Каждый объект класса - строчка в таблице
---


### Типы полей модели и их соответствие типам SQL
- CharField -> VARCHAR(N)
- EmailField -> VARCHAR(N)
- TextField -> LONGTEXT
- BooleanField -> TINYINT(1)
- DateTimeField -> DATETIME
- DateField -> DATE
- TimeField -> TIME
- ItegerField - INT(11)
- PostitiveIntegerField -> INT(11)
---


### Свойства полей
- max_length - максимальная длинна поля
- unique - значения в этом поле должны быть уникальны
- default - значение по-умолчанию
- blank - поле может быть пустым
- null - пустое поле хранится как NULL
- primary_key - первичный ключ
- auto_now - при каждом сохранение объекта в поле выставляется текущее время
- auto_now_add - при создании объекта в поле выставляется текущее время
---


### Связи между моделями
```py
from django.db import models

class Article(models.Model):
    ...

    category = models.ForeignKey(Category)
    tags = models.ManyToManyField(Tag)
```
---


### Связи между моделями
.left[![](./images/articles_diagram.png)]
---


### Миграции
Миграция - приведение схемы базы данных в соответствие с моделями

./manage.py makemigrations - анализ изменения в моделях и создание миграций
./manage.py migrate - применение миграций
