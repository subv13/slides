class: center, middle
# Работаем с базой данных
---


### SELECT
- Выбрать все записи  
SQL:
```sql
SELECT * FROM articles_article;
```
Python:
```py
Article.objects.all()
```

- Выбрать первые 10 записей  
SQL:
```sql
SELECT * FROM articles_article LIMIT 10;
```
Python:
```py
Article.objects.all()[:10]
```
---


### SELECT
- Выбрать все записи у которых title равен 'facilisi cras non'  
SQL:
```sql
SELECT * FROM articles_article WHERE title='facilisi cras non';
```
Python:
```py
Article.objects.filter(title='facilisi cras non')
```

- Выбрать все записи у которых title не равен 'facilisi cras non'  
SQL:
```sql
SELECT * FROM articles_article WHERE NOT title='facilisi cras non';
```
Python:
```py
Article.objects.exclude(title='facilisi cras non')
```
---


### SELECT
- Выбрать все записи у которых title содержит 'non'  
SQL:
```sql
SELECT * FROM articles_article WHERE title LIKE '%non%';
```
Python:
```py
Article.objects.filter(title__contains='non')
```

- Выбрать все записи c категорией 'sapien in sapien'  
SQL:
```sql
SELECT *
FROM articles_article AS aa
INNER JOIN articles_category AS ac ON aa.category_id = ac.id
WHERE ac.name = 'sapien in sapien';
```
Python:
```py
Article.objects.filter(category__name='sapien in sapien')
```
---

### SELECT
- Выбрать все записи с категорий 'sapien in sapien' и с title содержащим 'hac'
SQL:
```sql
SELECT *
FROM articles_article AS aa
INNER JOIN articles_category AS ac ON aa.category_id = ac.id
WHERE ac.name = 'sapien in sapien' AND aa.title LIKE '%hac%';
```
Python:
```py
Article.objects.filter(title__contains='hac',
                           category__name='sapien in sapien')
```
---


### SELECT
- Выбрать все записи, отсортировать по возрастанию даты создания  
SQL:
```sql
SELECT * FROM articles_article ORDER BY created_at;
```
Python:
```py
Article.objects.order_by('created_at')
```

- Выбрать все записи, отсортировать по убыванию даты создания  
SQL:
```sql
SELECT * FROM articles_article ORDER BY created_at;
```
Python:
```py
Article.objects.order_by('-created_at')
```
---


### QuerySet
QuerySet - объект, представляющий собой запрос к базе данных. QuerySet является ленивым (lazy) объектом - запрос в базу данных осуществляется не в момент создания QuerySet, а в момент итерации по нему или в момент вызова метода возвращающего результат.
---


### Функции для работы с QuerySet
- filter, exlude - фильтрация
- order_by - сортировка
- values - выборка отдельных колонок
- distinct - выборка уникальных значений
---


### Функции для работы с QuerySet приводящие к запросу в БД
- create - создание объекта
- update - обновление объектов
- delete - удаление объектов
- get - выборка объекта
- get_or_create - выборка объекта или его создание
- count - выборка количества объектов
---


### Синтаксис условий (field lookups)
- field=value - значение поля field точно совпадает с value
- field__contains=value - значение поля содержит value
- relation__field=value - значение поля field в связанной таблице содержит value
- field__gt - больше
- field__lt - меньше
- field__gte - больше или равно
- field__lte - меньше или равно
- field__in=[v1, v2, v3] - значение поля попадает в некоторый список значений
---


### Пример работы с объектами в базе
```py
from articles.models import Article, Category, Tag

category = Category.objects.get_or_create(name='Научпоп')

article = Article(title='Какое-то название статьи')
article.text = "Какой-то текст"
article.category = category
article.save()

tags = ['tag1', 'tag2', 'tag3']
for tag in tags:
    t = Tag.objects.create(name=tag)
    article.tags.add(t)
article.save()
```
---


### Другой пример работы с объектами в базе
```py
from articles.models import Article, Category

article = Article.objects.get(id=15).prefetch_related()

category = article.category
print category.name

for tag in article.tags.all():
    print tag.id, tag.name
    
category = Category.objects.get(name='Научпоп')

for article in category.article_set.all():
    print article.title, article.created_at
```
---
