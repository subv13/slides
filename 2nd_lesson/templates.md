class: center, middle
# Шаблонизация
---


### Как делать не нужно
```py
def article_detail(request, slug):
    try:
        article = Article.objects.get(slug=slug)
    except Article.DoesNotExist:
        raise Http404

    page = """
    <html>
      <head>
        <title>{}</title>
      </head>
      <body>
        {}
      </body>
    </html
    """

    return HttpResponse(page.format(article.title, article.text))
```
---


### Как нужно делать
Нужно отделать данные (контекст) от преставления (шаблона)
---


### Синтаксис шаблонов в Django
Шаблоны в Django это просто HTML-код с вставками (тэгами) шаблонизатора
```html
<html>
  <head>
    <title>
      { article.title }
    </title>
  </head>
  <body>
    { article.text }
  </body>
</html>
```
---


### Вызов Django-шаблонизатора во вьюхе
```py
from django.shortcuts import render, render_to_response

def article_detail(request, slug):
    try:
        article = Article.objects.get(slug=slug)
    except Article.DoesNotExist:
        raise Http404

    return render_to_response('articles/article_detail.html', {
        'article': article
    })

    # или

    return render(request, 'articles/article_detail.html', {
        'article': article
    })
```
---


### Откуда Django загружает шаблоны
Директории где Django будет искать шаблоны определяется в settings.py через переменную TEMPLATES

Существует два способа указать Django откуда загружать шаблоны
- Через список DIRS
- Через опцию APP_DIRS выставлена в True
```py
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'temlates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
            ...
            ],
        },
    },
]
```
---


### Возможности шаблонизатора
- {% for item in list %} {% endfor %} - проход по списку
- {% if var %} {% endif %} - условное отображение
- {% include "another.html" %} - включение подшаблона
- {{ var }} - вывод переменной
- {{ var|truncatechars:9 }} - применение фильтров
- {# comment #}, {% comment %} {% endcomment %} - комментарии
---


### Обращение к свойствам и методам
- {{ some_object.property }} - обращение к свойству
- {{ some_object.func }} - вызов функции
- {{ some_dictionary.key }} - обращение по ключу в словаре
- {{ some_list.0 }} - обращение к первому элементу массива
---


### Наследование шаблонов
#### Шаблон base.html
```html
<html>
  <head>
    <title>{% block title %}{% endblock %}</title>
    {% block extrahed %}{% endblock %}
  </head>
  <body>
    {% block content %}{% endblock %}
  </body>
</html>
```
---


### Наследование шаблонов
#### Шаблон index.html
```html
{% extends "base.html" %}

{% block title %}
  Главная
{% endblock %}

{% block content %}
  <ul>
    {% for item in list %}
      <li>{{ item.text }}</li>
    {% endfor %}
  <ul>
{% endblock %}
```
---

### Плюсы
- Верстка и код расположены отдельно
- Верстку можно использовать повторно
- В python-коде не будет HTML-кода
