### Презентации для SMENASCHOOL

Сделаны с помощью [remarkjs] [1]

Просматривается в браузере с помощью:

- Пакета http-server для nodejs (http-server .)
- Пакета http для python3 (python3.5 -m http.server)
- Любого другого сервера отдающего статику


[1]: https://www.google.ru/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiS3tmynpLUAhVBlCwKHXS3CFkQFggkMAA&url=https%3A%2F%2Fgithub.com%2Fgnab%2Fremark&usg=AFQjCNExRa8Ogwil9H5vYmneu0jk1aphig&sig2=PDvlFeHyk2fO5eU8zYNlaw
